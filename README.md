# Key-Api-Sample

Before you start go to [http://api.enkoda.pro] and create an Administrator's Account

## Detailed documentation/Examples: 
[https://gitlab.com/jarek707/key-api-sample/blob/master/DOCUMENTATION.md]

#### 1. Start with
```sh
git clone git@gitlab.com:jarek707/key-api-sample.git
cd key-api-sample
npm install
npm run bg
// Your should now connect from `http://localhost`
```

#### Optionally you can automatically run it in the browser:

```sh
npm run browser
```

#### To run the server on a different port copy `sample-config.json` file to `config.json` and set the port number
```sh
{
    "port": 8181
}
```
