# INTRO 

* SimpleBaas API is a `B`ackend `A`s `A` `S`ervice Platform providing data services to Front End developers who do not wish to build and maintain their own Backends.
* Additionally, a CMS service is provided for those developer who are familiar with HTML and CSS but may not have Javascript skills to confiugre data related tasks.

## Getting started

* Create a developer account at [http://simplebaas.com].
* Once you have created your account you will have a default App named `DEVELOPER` with an `appKey`. 
 
![alt text](https://simplebaas.com/assets/images/shot.png)

Copy the script tag (see image above) and put it in the `<head>` section of your index.html

```sh
    <script baas-public-api-key="<*appKey>" src="https://d1590ot8b7g2in.cloudfront.net/sdk.min.js" type="text/javascript"></script>
    <!-- example: -->
    <script baas-public-api-key="826e61cc-385f-6147-891a-8ed3b056a954" src="https://d1590ot8b7g2in.cloudfront.net/sdk.min.js"  type="text/javascript"></script>
```

#### At this point you can access you data (read-only)

```javascript
var sdk = SIMPLE_BAAS;
// You retrieve all your data with:
sdk.getAll()
.then( function(data) { console.log(data); });

// You retrieve all your keys (returns array of keys):
sdk.getKeys()
.then( function(data) { console.log(data); });

// Retrieve one Data Item for a specific Key
sdk.getItem("myFirstKey")
.then( function(data) { console.log(data); } );

```

#### In order to `set` or `delete` data items you need to be logged in

```javascript
sdk.login(email, password)
.then( function(userLoginInfo) {
    console.log(userLoginIfo);
});

// Once the SDK is ready and you are logged in
// you will be able to add, update and delete data
// using setItem(), and deleteItem()

let data =  {"first": "Jo", "last": "Blo", "credits" : 5};
sdk.setItem('anotherKey', data )
.then( function(addResponse) {
    data.content.credits = 10;
    return KEY_API_SDK.set('anotherKey', data.content );
})
.then( function(updateResponse) {
    console.log( updateResponse );
});

sdk.deleteItem("anotherKey")
.then( function(response) { console.log(response) } );

```
## The `heartbeat` Call

The `sdk.heartbeat` call returns the state of your login connection.
If the session is active (i.e the user is logged in) `userLoginInfo` will contain data of the logged in user.
The absence of user data (i.e. `user: null` in response) indicates that the session has expired (the user is not logged in).

```javascript
    sdk.heartbeat( function(userLoginInfo) { console.log( userLoginInfo ) });
```

# CDN Hosting
    
**PLASE NOTE**: CDN _checkbox indicates whether or not the data should be available on a public CDN server

Currently you can access this data on `https://d1590ot8b7g2in.cloudfront.net/clientUid/key` if the CDN box was checked.

# PERMISSIONS, PRIVACY & SECURITY
... new doc coming soon

# ADVANCED 

## REST and CORS
    
Each of the SDK calls corresponds to a specific REST call to the API server.
Calling the REST API directly, let's say using jQuery, may result in **CORS** issues.
The same API REST call using SIMPLE_BAAS will not have **CORS** issues

```javascript 
sdk.login(email, password) 
    -> $.get('http://simplebaas.com/auth/login/email/password') (CORS)
    -> sdk.get('http://simplebaas.com/auth/login/email/password') (no CORS)
    
sdk.logout() -> $.get('http://simplebaas.com/auth/logout')

sdk.getDataItem("myKey")
    -> $.get('http://simplebaas.com/data/c827501c-e05f-4fbd-2e7f-593d5b6eac69/myKey')

sdk.deleteDataItem("myKey")
    -> $.delete('http://simplebaas.com/data/c827501c-e05f-4fbd-2e7f-593d5b6eac69/myKey')

sdk.addDataItem( data ) -> $.post('http:/simplebaas.com/data', data);
sdk.updateDataItem( data ) -> $.put('http://simplebaas.com/data', data);
```
